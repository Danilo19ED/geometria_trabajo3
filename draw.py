import pygame
import random
NEGRO = (0, 0, 0)
ROJO = (255, 0, 0)
CAFE = (90, 50, 15)
VERDE = (10, 255, 10)
BLANCO = (255, 255, 255)
pygame.init()
Dimensiones = (600, 600)
Pantalla = pygame.display.set_mode(Dimensiones)
pygame.display.set_caption("Plano Cartesiano")

#CREO EL TEXTO DEL PLANO CARTESIANO
Fuente = pygame.font.Font(None, 20)
Texto = Fuente.render("Representacion Grafica del Plano Cartesiano", True, BLANCO)
X= Fuente.render("X", True, BLANCO)
Y = Fuente.render("Y", True, BLANCO)
Cuadrado =Fuente.render("250 X 120", True, BLANCO)
Uno = Fuente.render("1", True, BLANCO)
Dos = Fuente.render("2", True, BLANCO)
Uno1 = Fuente.render("1", True, BLANCO)
Dos2 = Fuente.render("2", True, BLANCO)
Var = Fuente.render("3", True, BLANCO)
Var2 = Fuente.render("2", True, BLANCO)

import pygame
import math

NEGRO = (0, 0, 0)
ROJO = (255, 0, 0)
CAFE = (90, 50, 15)
VERDE = (10, 255, 10)
BLANCO = (255, 255, 255)
pygame.init()
Dimensiones = (600, 600)
Pantalla = pygame.display.set_mode(Dimensiones)
pygame.display.set_caption("Plano Cartesiano")

class Punto:

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
           return 'I'
        elif self.x < 0 and self.y > 0:
            return 'II'
        elif self.x < 0 and self.y < 0:
            return 'III'
        elif self.x > 0 and self.y < 0:
            return 'IV'
        elif self.x != 0 and self.y == 0:
            return "{} se sitúa sobre el eje X"
        elif self.x == 0 and self.y != 0:
            return "{} se sitúa sobre el eje y"
        else:
            return "{} se sitúa sobre el origen"

    def vector(self, p):
        v = Punto(p.x - self.x, p.y - self.y)
        return v

    def distancia(self, p):
        d = math.sqrt( (p.x - self.x)**2 + (p.y - self.y)**2 )
        return d

    def __str__(self):
        return "({}, {})".format(self.x, self.y)



class Rectangulo:
    pi= None
    pf= None

    def __init__(self, pi, pf):
        self.pi = pi
        self.pf = pf


    def base(self):
        return self.pf.x - self.pi.x


    def altura(self):
        return self.pf.y - self.pi.y

    def area(self):
        return self.base()*self.altura()


if __name__ == '__main__':

    A = Punto(2,3)
    B = Punto(5,5)
    C = Punto(-3, -1)
    D = Punto(0,0)

    print (f"El punto {A} se encuentra en el cuadrante {A.cuadrante()}")
    print (f"El punto {C} se encuentra en el cuadrante {C.cuadrante()}")
    print (f"El punto {D} se encuentra {D.cuadrante()}")

    print (f"La distancia entre el punto {A} y {B} es {A.distancia(B)}")
    print (f"La distancia entre el punto {B} y {A} es {B.distancia(A)}")

    da= A.distancia(D)
    db= B.distancia(D)
    dc= C.distancia(D)
    if da > db and da > dc:
        print (f"El punto {A} se encuentra más lejos del origen")
    elif db > da and db > dc:
        print(f"El punto {B} se encuentra más lejos del origen")
    else:
        print(f"El punto {C} se encuentra más lejos del origen")

    rect = Rectangulo(A, B)
    print("La base del rectángulo es {}".format(rect.base()))
    print("La altura del rectángulo es {}".format(rect.altura()))
    print("El área del rectángulo es {}".format(rect.area ()))

Fuente = pygame.font.Font(None, 20)
Texto = Fuente.render("Plano Cartesiano", True, BLANCO)
X= Fuente.render("X", True, BLANCO)
Y = Fuente.render("Y", True, BLANCO)
Cuadrado =Fuente.render("250 X 120", True, BLANCO)
Uno = Fuente.render("-1", True, BLANCO)
Dos = Fuente.render("-2", True, BLANCO)
Uno1 = Fuente.render("1", True, BLANCO)
Dos2 = Fuente.render("2", True, BLANCO)
Var = Fuente.render("250", True, BLANCO)
Var2 = Fuente.render("120", True, BLANCO)

Terminar = False
reloj = pygame.time.Clock()

while not Terminar:
     for Evento in pygame.event.get():
        if Evento.type == pygame.QUIT:
            Terminar = True

     Pantalla.fill(NEGRO)
     pygame.draw.line(Pantalla, BLANCO, [595,300],[5,300],3)
     pygame.draw.line(Pantalla, BLANCO, [300, 5], [300, 595], 3)
     pygame.draw.rect(Pantalla, ROJO, (300, 300, 250, 120), 0)
     Pantalla.blit(Texto, [10, 10])
     Pantalla.blit(X, [585, 280])
     Pantalla.blit(Y, [310, 585])
     Pantalla.blit(Uno, [290, 310])
     Pantalla.blit(Dos, [290, 340])
     Pantalla.blit(Uno1, [310, 280])
     Pantalla.blit(Dos2, [330, 280])
     Pantalla.blit(Var, [540, 280])
     Pantalla.blit(Var2, [275, 410])
     Pantalla.blit(Cuadrado, [400, 350])



     pygame.display.flip()
     reloj.tick(20)
